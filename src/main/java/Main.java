import org.junit.Assert;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Map<Integer, String> stringStringMap = new HashMap<>();

        stringStringMap.put(1, "Nas is God of all humans");
        stringStringMap.put(2, "Nas is God for only me");
        stringStringMap.put(3, "Nas is just human");
        stringStringMap.put(4, "Nas is not human");

        // Retrieving All keys
        Set entrySet = stringStringMap.entrySet();
        // Retrieving All values
        Collection collections = stringStringMap.values();

        // Retrieving any key with value containing "Nas is God"
        List<Integer> keyList = stringStringMap.entrySet().stream()
                       .filter(es -> es.getValue().contains("Nas is God"))
                       .map(Map.Entry::getKey)
                       .collect(Collectors.toList());

        Assert.assertTrue(keyList.contains(1));
        Assert.assertTrue(keyList.contains(2));
        Assert.assertEquals(2, keyList.size());
        // Retrieving the first key with value containing "Nas is God"
        Optional<Integer> firsBeliever = stringStringMap.entrySet().stream()
                                        .filter(es-> es.getValue().contains("Nas is God"))
                                        .map(Map.Entry::getKey)
                                        .findFirst();
        // FirstBeliever is optional,if there is no value matching with Nas is God the keyValue
        // will be -1
        Integer firstKey = firsBeliever.orElse(-1);
        Assert.assertEquals(1, firstKey.intValue());
    }
}
